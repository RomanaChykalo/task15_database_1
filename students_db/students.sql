-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema students_study
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema students_study
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `students_study` DEFAULT CHARACTER SET utf8 ;
USE `students_study` ;

-- -----------------------------------------------------
-- Table `students_study`.`arrears`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`arrears` (
  `arrear` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`arrear`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`region` (
  `code` INT(11) NOT NULL,
  `region` VARCHAR(50) NOT NULL,
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`city` (
  `city` VARCHAR(50) NOT NULL,
  `region_id` INT(11) NOT NULL,
  `id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_region`
    FOREIGN KEY (`id`)
    REFERENCES `students_study`.`region` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`groups` (
  `name` VARCHAR(20) NOT NULL,
  `number` INT(11) NOT NULL,
  `year_of_admission` DATE NULL DEFAULT NULL,
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`secondary_education`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`secondary_education` (
  `institution_name` VARCHAR(50) NOT NULL,
  `telephone` VARCHAR(10) NULL DEFAULT NULL,
  `rector_fullname` VARCHAR(60) NULL DEFAULT NULL,
  `id` INT NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_secondary_education_city1_idx` (`city_id` ASC),
  CONSTRAINT `fk_secondary_education_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `students_study`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`students`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`students` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NOT NULL,
  `rating` INT(11) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `date_of_admission` DATE NOT NULL,
  `card_number` INT(11) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `city_id` INT(11) NOT NULL,
  `secondary_education_id` INT(11) NOT NULL,
  `groups_id` INT(11) NOT NULL,
  `full_name` varchar(45) GENERATED ALWAYS AS (concat(`name`,' ',`middle_name`,' ',`surname`)) VIRTUAL,
  `card_number_and_admission` varchar(60) GENERATED ALWAYS AS (concat(`card_number`,' ',year(`date_of_admission`))) VIRTUAL,
  `entry_age` int(11) GENERATED ALWAYS AS ((year(`date_of_admission`) - year(`date_of_birth`))) VIRTUAL,
  
  PRIMARY KEY (`id`),
  INDEX `city_id_idx` (`city_id` ASC),
  CONSTRAINT `city_id`
    FOREIGN KEY (`city_id`)
    REFERENCES `students_study`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `secondary_ed`
    FOREIGN KEY (`id`)
    REFERENCES `students_study`.`secondary_education` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `group_id`
    FOREIGN KEY (`id`)
    REFERENCES `students_study`.`groups` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `students_study`.`students_has_arrears`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_study`.`students_has_arrears` (
  `students_id` INT(11) NOT NULL,
  `arrear` VARCHAR(50) NOT NULL,
  INDEX `fk_students_has_arrears_arrears1_idx` (`arrear` ASC),
  INDEX `fk_students_has_arrears_students1_idx` (`students_id` ASC),
  CONSTRAINT `fk_students_has_arrears_arrears1`
    FOREIGN KEY (`arrear`)
    REFERENCES `students_study`.`arrears` (`arrear`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_has_arrears_students1`
    FOREIGN KEY (`students_id`)
    REFERENCES `students_study`.`students` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
