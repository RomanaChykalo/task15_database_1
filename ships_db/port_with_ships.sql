create database port_with_ships;
use port_with_ships;
create table classes(class_name varchar(50) not null, type varchar(50), country varchar(50), num_gans varchar(50), bore real, displacement int);
create table battles (name varchar (20) NOT NULL,date datetime NOT NULL);
create table ships(name varchar(50) not null, launched int null,class_name varchar(50));
create table outcomes (result varchar(10) not null, ship varchar(50) not null, battle varchar(20) not null);
alter table battles  add constraint pk_battles primary key clustered (name);
alter table classes add constraint pk_classes primary key clustered (class_name);
ALTER TABLE classes change name class_name varchar(50);
alter table ships add constraint pk_ships primary key clustered (name);
alter table outcomes add constraint pk_outcomes primary key clustered (ship,battle);
alter table ships add constraint fk_ships_classes foreign key (class_name) references classes(class_name);
alter table outcomes add constraint fk_outcomes_battles foreign key (battle) references battles(name);

insert into classes values('Kongo', 'bc', 'Japan', 8, 5, 15);
insert into classes values('Mongo','bb', 'Ukraine', 5, 20, 50);
insert into classes values('Pinokio','bc', 'Germany', 9, 20, 45000);
insert into classes values('Chili','bb', 'Poland', 5, 10, 65000);
insert into classes values('Kivi','bb', 'Ukraine', 9, 15, 55000);
insert into classes values('Oland','bc', 'Japan', 9, 20, 50000);
insert into classes values('Bingo','bb', 'USA', 9, 20, 25000);

insert into ships values('California', 1921, 'Kongo');
insert into ships values('Haruna',1916, 'Chili');
insert into ships values('Hiei',1914, 'Bingo');
insert into ships values('Iowa',1943, 'Oland');
insert into ships values('Kirishima',1915, 'Pinokio');
insert into ships values('Kon',1913,'Pinokio');
insert into ships values('Missouri',1944, 'Chili');
insert into ships values('Musashi',1942, 'Bingo');
insert into ships values('New Jersey',1943, 'Kivi');
insert into ships values('North Carolina',1941, 'Kivi');
insert into ships values('Ramillies',1917,'Oland');
insert into ships values('Renown',1916, 'Oland');
insert into ships values('Repulse',1916, 'Pinokio');
insert into ships values('Resolution',1916, 'Pinokio');
insert into ships values('Revenge',1916, 'Mongo');
insert into ships values('Royal Oak',1916, 'Mongo');
insert into ships values('Royal Sovereign',1916, 'Pinokio');
insert into ships values('Tennessee',1920, 'Pinokio');
insert into ships values('Washington',1941, 'Oland');
insert into ships values('Wisconsin',1944, 'Oland');
insert into ships values('Yamato',1941, 'Mongo');
insert into ships values('South Dakota',1941, 'Mongo'); 

insert into battles values('Guadalcanal','1942-11-15 00:00:00.000');
insert into battles values('North Atlantic','1941-05-25 00:00:00.000');
insert into battles values('North Cape','1943-12-26 00:00:00.000');
insert into battles values('Surigao Strait','1944-10-25 00:00:00.000');
insert into battles values ('#Cuba62a'   , '1962-10-20');
insert into battles values ('#Cuba62b'   , '1962-10-25');

insert into outcomes values('sunk','California','Surigao Strait' );
insert into outcomes values('OK', 'New Jersey', 'North Cape');
insert into outcomes values('OK','Musashi','Guadalcanal');
insert into outcomes values('sunk','Tennessee','North Atlantic' );
insert into outcomes values('sunk', 'Royal Oak','#Cuba62b');
insert into outcomes values('OK', 'Revenge','#Cuba62b');
insert into outcomes values('sunk', 'Repulse','#Cuba62a');
insert into outcomes values('damaged', 'North Carolina', '#Cuba62a');
insert into outcomes values('OK','Bimbi', 'Surigao Strait');
insert into outcomes values('sunk', 'Renown', 'Surigao Strait');
insert into outcomes values('damaged', 'Yamato', 'Guadalcanal');
insert into outcomes values('OK', 'South Dakota', 'Guadalcanal');
insert into outcomes values('OK', 'South Dakota', 'North Cape');
insert into outcomes values('OK', 'Resolution','North Cape');
insert into outcomes values('sunk','Royal Oak','North Cape');
insert into outcomes values('damaged','Royal Sovereign', '#Cuba62a');

select nabattlesme, class_name from ships order by name;
select class_name from classes where country like 'Japan' order by type;
select name, launched from ships where launched>=1920 and launched<=1942  order by launched desc;
select ship, battle, result from outcomes where result!='sunk' and battle='Guadalcanal' order by ship desc;
select  ship, battle, result from outcomes where result like 'sunk' order by ship desc;
select class_name, displacement from classes where displacement>=40 order by type;


select name from ships where name rlike '^R' and name rlike 'n$';
select name from ships where name rlike ('ll');
select name, launched from ships where name not rlike 'a';
select name from battles where name rlike ' ' and name not rlike 'c';

select country, type from classes where type='bb' and type='bc';
select name, displacement from ships s join classes c on s.class_name=c.class_name;
select ship,  battle, date from battles b join outcomes o on b.name=o.battle where o.result='OK';
select name, country from ships s right join classes c on s.class_name=c.class_name;